<?php 
/**
 * PHP 7.2
 */
error_reporting(-1);
set_time_limit(0);
ini_set('memory_limit','-1');

$fileName = isset($argv[1]) ? $argv[1] : false;

if ($fileName === false) {
    exit('Fail. Input file name.');    
}

$fileFullPath = __DIR__.'/storage/'.$fileName;
if (!is_file($fileFullPath)) {
    exit('Fail. File Not Found.');
}   

/**
 * in_array 검색을 피하기 위해 hashmap 형태로 바꾼다.
 * 3배 속도 차이.
 */
$getFileLine = function (string $fileFullPath)  {
    $file =  fopen($fileFullPath, 'r');
    
    $i = 0;
    while ( ($line = fgets($file)) !== false) {
        yield $i => array_fill_keys(explode(' ', $line), true);
        $i++;
    }

    fclose($file);
}; 

$checkHash = [];
foreach($getFileLine($fileFullPath) as $key => $line) {
    if ($key === 0) {
        continue;
    }    
    $checkHash[] = $line;
}
unset($getFileLine);

$startTime = microtime(true);

/**
 * 결과 저장 파일
 */
$resultFileName = 'result_'.microtime().'.txt';
$resultFile = fopen(__DIR__.'/storage/'.$resultFileName, 'w') or exit('Fail. Result File Permission');

foreach($checkHash as $key => $hash) {
    $count = 0;
    $resultArray = [];

    foreach($checkHash as $subKey => $subHash) {
        if ($key === $subKey) {
            continue;    
        }

        $equalCount = 0;
        foreach($hash as $checkDiffKey => $ckeckDiffValue) {
            if (isset($subHash[$checkDiffKey])) { 
                $equalCount++;
            }
        }

        if ($equalCount === 0) {
            continue;
        }
     
        if ($count < $equalCount) {
            $resultArray = [ $key.'-'.$subKey ];
            $count  = $equalCount;  
        } 
        else if ($count === $equalCount) {
            $resultArray[] = $key.'-'.$subKey;     
        }
    }
    fwrite($resultFile, implode(",", $resultArray)."\n");
}
unset($checkCountArray);

fclose($resultFile);

$endTime = microtime(true);

$resultTime = $endTime - $startTime;

echo "시작 : ".$startTime."\n";
echo "종료 : ".$endTime."\n";
echo "작업시간 : ".$resultTime."\n";
echo "결과파일 : ".$resultFileName."\n";
exit;