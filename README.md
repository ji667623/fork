멀티 프로세싱과 싱글 프로세싱 알고리즘 작성
========================================

테스트 파일 
```
storage/
    *.txt
```

멀티 프로세싱
```
match_multi.php
```

싱글 프로세싱
```
match.php
```

실행 
```
php 실행파일 테스트 파일

ex) php match_multi.php 300000.txt
```

매칭 데이터가 작다면 싱글 프로세싱 이 더 빠를 수 있지만 
매칭 데이터가 커 질 수록 멀티 프로세싱의 결과 속도가 월등하게 차이나게 된다.

실행 환경
    Windows 10에서 지원하는 Ubuntu 18.04  LTS 
    PHP 7.2 None Thread Safe (NTS)

#### `php의 fork 관련 함수는 유닉스 계열에서만 동작 하므로 윈도우에서 실행 할수 없습니다.`
#### `windows10을 쓴다면 windows10 에서 지원하는 ubuntu 설치를 권장 드립니다.`
#### [Windows10 Ubuntu 설치](https://hiseon.me/windows/windows10-ubuntu-install/)     

실행 결과
![실행결과](./assets/result.png)  
실행 결과 수정  
```
ji6676@DESKTOP-3UJAOOC:/mnt/e/newprojcet$ php match.php 20000.txt
시작 : 1571489872.4661
종료 : 1571490104.1005
작업시간 : 231.63444709778
결과파일 : result_0.46605400 1571489872.txt
ji6676@DESKTOP-3UJAOOC:/mnt/e/newprojcet$ php match_multi.php 20000.txt
시작 : 1571490486.7045
종료 : 1571490677.1007
작업시간 : 190.39621710777
결과파일 : result_0.70446500 1571490486.txt
ji6676@DESKTOP-3UJAOOC:/mnt/e/newprojcet$
```


싱글 프로세싱 CPU 사용량  
    ![싱글 프로세싱 CPU 사용량](./assets/singlecpu.png)

멀티 프로세싱 CPU 사용량   
    ![멀티 프로세싱 CPU 사용량](./assets/multicpu.png)



