<?php 
/**
 * PHP 7.2
 */
error_reporting(-1);
set_time_limit(0);
ini_set('memory_limit','-1');

/**
 * 멀티 프로세싱은 윈도우 OS에서 지원하지 않는다.
 */
if (strpos(PHP_OS, 'WIN') !== false) {
    exit('Fail. Window not support.');    
}

$fileName = isset($argv[1]) ? $argv[1] : false;
/**
 * 파일 명 체크
 */
if ($fileName === false) {
    exit('Fail. Input file name.');    
}

/**
 * 파일 체크 
 */
$fileFullPath = __DIR__.'/storage/'.$fileName;
if (!is_file($fileFullPath)) {
    exit('Fail. File Not Found.');
}   

/**
 * in_array 검색을 피하기 위해 hashmap 형태로 바꾼다.
 * 3배 속도 차이.
 */
$getFileLine = function (string $fileFullPath)  {
    $file =  fopen($fileFullPath, 'r');

    $i = 0;
    while ( ($line = fgets($file)) !== false) {
        yield $i => array_fill_keys(explode(' ', $line), true);
        $i++;
    }

    fclose($file);
}; 

$checkHash = [];
foreach($getFileLine($fileFullPath) as $key => $line) {
    if ($key === 0) {
        continue;
    }    
    $checkHash[] = $line;
}
unset($getFileLine);

/**
 * CPU 갯수
 */
$pathCpuInfo = '/proc/cpuinfo';
$processCount = !file_exists($pathCpuInfo) ? 1 : substr_count(file_get_contents($pathCpuInfo), 'processor'); 

$startTime = microtime(true);

$resultArray = [];

/**
 * 결과 저장 파일
 */
$resultFileName = 'result_'.microtime().'.txt';
$resultFile = fopen(__DIR__.'/storage/'.$resultFileName, 'w') or exit('Fail. Result File Permission');

/**
 * job lambda
 */
$jobFunction = function (int $key, array $hash)  use ($checkHash, & $resultFile) {
    $pid = pcntl_fork();
    
    if ($pid === -1) {
        exit('Fail. Fork Error.');     
    } 
    
    if ($pid === 0) {
        $count = 0;
        $checkArray = [];

        foreach($checkHash as $subKey => $subHash) {
            if ($key === $subKey) {
                continue;    
            }

            $equalCount = 0;
            foreach($hash as $checkDiffKey => $ckeckDiffValue) {
                if (isset($subHash[$checkDiffKey])) { 
                    $equalCount++;
                }
            }

            if ($equalCount === 0) {
                continue;
            }
         
            if ($count < $equalCount) {
                $checkArray = [ $key.'-'.$subKey ];
                $count  = $equalCount;  
            } 
            else if ($count === $equalCount) {
                $checkArray[] = $key.'-'.$subKey;     
            }
        }
        fwrite($resultFile, implode(",", $checkArray)."\n");
        exit();
    }
};


$jobSize = sizeof($checkHash) - 1;
for ($i=0; $i<$processCount; $i++) {
    if ($i > $jobSize) {
        break;    
    }
    $jobFunction($i, $checkHash[$i]);
}

while (pcntl_waitpid(0, $status) !== -1) {
    if ($i  >  $jobSize) {
        continue;    
    } 
    $jobFunction($i, $checkHash[$i]);
    $i++;
}
unset($jobFunction);

fclose($resultFile);

$endTime = microtime(true);

$resultTime = $endTime - $startTime;

echo "시작 : ".$startTime."\n";
echo "종료 : ".$endTime."\n";
echo "작업시간 : ".$resultTime."\n";
echo "결과파일 : ".$resultFileName."\n";
exit;